# guest_swagg.cgi

The source for the SwaggNet Guestbook CGI script
(http://swagg.net/cgi-bin/guest.cgi). Posts guestbook messages to
Discord Webhook URL.

This script wants to run alongside three dot files:

1. .tom.url

   The Discord Webhook URL to use

1. .name.bans

   List of "name" values to silently ban from posting (provides
   feedback saying "thanks" but doesn't actually post) Regexp is
   supported

1. .msg.bans

   List of words or phrases banned from message body. Regexp is
   supported

This script relies on CGI.pm and WebService::Discord::Webhook at minimum

## Discord Webhook URLs

Your Discord Webhook URL should look something like this:

    https://discordapp.com/api/webhooks/740722033561960495/QZTRS0iCgFM531fY2DHWwtalOjdbyPY_waRJY8eDqeWtCTdwPNJmaPkMd1Er-a2ebD5E

I generate Webhook URLs through the Discord client application
(<http://discord.com/app> should work too) by clicking on the server
name > “Server settings” > “Integrations” > “Webhooks” > “New
Webhook”. More information on Discord Webhooks can be found
[here](https://discord.com/developers/docs/resources/webhook).
